import path from 'path';
import chalk from 'chalk';
import ora from 'ora';

// LOCAL
import Bitbucket from './bitbucket';
import Git from './git';

/* setTimeout(() => {
spinner.text = 'Loading rainbows';
}, 1000);

setTimeout(() => {
  spinner.stop();

  //process.stdout.write('\n');
  process.exit(0);
}, 3000); */


/**
 * Boilerplate Classe
 *
 * @version 0.1.0
 * @author Roger Luiz <rogerluizm@gmail.com>
 *
 * @see create()
 * @see refreshToken()
 */
class Boilerplate {
  /**
   * Constructor for {@link Boilerplate}
   *
   * @constructor
   * @param {Object} options  Lista com as configurações do repositorio e do boilerplate
   */
  constructor(options) {
    // Inicia o loader spinner
    console.log();
    console.log('------------------------');
    console.log();

    this.spinner = ora('-');
    this.spinner.color = 'yellow';
    this.spinner.start();

    this.options = options;
    this.bitbucket = new Bitbucket(this.options, this.spinner);
    this.git = new Git(this.bitbucket.config, this.spinner);
  }

  /**
   * Cria o projeto no bitbucket e configura todo o projeto
   *
   * @public
   */
  async create() {
    const {
      options,
      bitbucket,
      git,
      spinner,
    } = this;

    try {
      await bitbucket.refreshToken();
      const {
        links
      } = await bitbucket.createRepository();
      bitbucket.activePipelines();

      const {
        protocol,
        git_username,
        basedir,
        reponame,
        segment,
        globaldir,
        slugname,
      } = options;
      const cloneDir = path.join(basedir, reponame);
      const cloneURL = (protocol === 'ssh') ? links.clone[1].href : links.clone[0].href.replace('//onedigital', `//${git_username}`);

      if (segment) {
        await git.clone(
          cloneURL,
          path.normalize(cloneDir),
          path.normalize(globaldir),
          [slugname, segment],
        );
      } else {
        await git.clone(cloneURL, path.normalize(cloneDir), path.normalize(globaldir));
      }


      await Promise.all([
        bitbucket.webhook(),
        bitbucket.addSSHkeys(),
        bitbucket.addKnownHosts(),
        bitbucket.addVariables(),
      ]);

      spinner.start('Finalizando..');
      spinner.succeed('Projeto gerado com sucesso :)');

      console.log();
      console.log('------------------------');
      console.log();

      return {
        success: true
      };
    } catch (error) {
      console.log(error);
      spinner.fail(`ocorreu um erro: ${error}`);
      return error;
    }
  }

  /**
   * Gera um novo token de acesso
   *
   * @public
   */
  async refreshToken() {
    const {
      bitbucket
    } = this;

    const {
      access_token
    } = await bitbucket.refreshToken();

    console.log(`${chalk.green('Token atualizado com sucesso access_token: \n')}`);
    console.log(`${chalk.blue(access_token)}`);
  }
}

module.exports = options => new Boilerplate(options);
