#Generator Boilerplate Command-Line
[![npm package](https://img.shields.io/badge/npm-v5.6.0-green.svg)](https://npm.org) [![nodeJs](https://img.shields.io/badge/node-v8.10.0-green.svg)](https://nodejs.org) ![buil passing](https://img.shields.io/scrutinizer/build/g/filp/whoops.svg) ![licenca](https://img.shields.io/npm/l/express.svg)

### instalando dependecias
```sh
$ npm install
```

### criando um link para o cli

```sh
$ cd generator-boilerplate-cli
$ npm link
```

### agora o comando "one-cli" pode ser usando 

```sh
$ cd ../generator-boilerplate-cli
$ one-cli -h #vai printar infos do help 
```

### criando um projeto
```sh
$ one-cli create
```
- Responda as perguntas e o repositorio sera criado com as informações passadas e template escolhido
- Tambem sera feito um clone na maquina baseado no usuario do bitbucket

#### perguntas
```sh
$ #? Entre com o nome do repositorio: <00_000_J2_nome_projeto>
$ #? Entre com um slug para o repositorio: <nome-projeto>
$ #? Entre com o seu usuario do Bitbucket: <usuario bitbucket>
$ #? Entre com o link da task: <https://jobs.one.com.br/id_job>
$ #? Entre com o link de produção do projeto: <https://banco.bradesco/projeto_url>
$ #? Escolha um cliente para este projeto: <lista de clientes>
$ #? Qual tipo de projeto deseja criar? <lista dos templates>
$ #? Qual será o segmento? <Classic>, <Exclusive>, <Prime>,
$ #? Qual protocolo você usa? <https> ou <ssh> 
```

### Enviando um pacote para o  OwnCloud
- Esse comando ira criar um zip a partir do diretorio <dir> com o nome dado <zip-name>.zip
```sh
$ one-cli publish <dir> <zip-name>
```

### Enviando um pacote e adicionando a planilha
- Esse comando so vai funcionar quando tiver no pipelines, sendo que ele pega as variais de ambientes que estão no bitbucket
```sh
$ one-cli send
```

## Alguns comandos utils
- one-cli --help mostra todos os metodos que existe no CLI
```sh
$ one-cli -h
$ one-cli send -h
$ one-cli publish -h 
$ one-cli crate -h
```

