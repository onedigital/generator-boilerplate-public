import request from 'superagent';
import { isTest, sendPostRequest } from './utils';

/**
 * Bitbucket utils
 *
 * @version 0.1.0
 * @author Roger Luiz <rogerluizm@gmail.com>
 *
 * @see getToken()
 * @see refreshToken()
 * @see getRepository()
 * @see createRepository()
 * @see webhook()
 * @see activePipelines()
 * @see addSSHkeys()
 * @see addKnownHosts()
 * @see addVariables()
 */
class Bitbucket {
  /**
   * Constructor for {@link Bitbucket}
   *
   * @constructor
   * @param {Object} options Lista com as configurações do repositorio e do boilerplate
   */
  constructor(options = {}, spinner) {
    this.bitbucket_url = 'https://api.bitbucket.org/2.0/repositories';
    this.token = '';
    this.spinner = spinner;
    this.config = { bitbucket: Object.assign(options.BITBUCKET_KEYS, options.bitbucket) };
    this.PIPELINES_PUBLIC_KEY = options.PIPELINES_PUBLIC_KEY;
    this.HOSTNAME = options.HOSTNAME;
    this.config.repository = options;
    this.private_key = options.private_key;
    this.public_key = options.public_key;
  }

  doActionWithSpinner(text, action) {
    if (isTest()) this.spinner[action](text);
  }

  startSpinner(text) {
    this.doActionWithSpinner(text, 'start');
  }

  succeedSpinner(text) {
    this.doActionWithSpinner(text, 'succeed');
  }

  failSpinner(text) {
    this.doActionWithSpinner(text, 'fail');
  }

  getAuthorization() {
    return `Bearer ${this.token}`;
  }

  getGitUrl() {
    return `${this.bitbucket_url}/${this.config.bitbucket.username}/${this.config.repository.reponame.toLowerCase()}`;
  }

  /**
   * Gera um novo token do bitbucket
   *
   * @public
   * @return {Promise} Retorna uma promise com o resultado do token
   */
  getToken() {
    const self = this;
    const { client_id, secret, grant_type } = this.config.bitbucket;
    const url = `https://${client_id}:${secret}@bitbucket.org/site/oauth2/access_token`;

    this.startSpinner('Gerando um novo token.');

    return new Promise((resolve, reject) => {
      request
        .post(url)
        .send(`grant_type=${grant_type}`)
        .end((err, res) => {
          if (err) {
            this.failSpinner('Nao foi prossivel gerar o token');

            reject(err);
          }

          self.token = res.body.access_token;
          this.succeedSpinner(`Token ${self.token} gerado com sucesso.`);

          resolve(res.body);
        });
    });
  }

  /**
   * Faz o refresh do token do bitbucket
   *
   * @public
   * @return {Promise} Retorna uma promise com o resultado do token
   */
  refreshToken() {
    const self = this;
    const {
      client_id, secret, refresh_token,
    } = this.config.bitbucket;
    const url = `https://${client_id}:${secret}@bitbucket.org/site/oauth2/access_token`;

    this.startSpinner('Atualizado token gerando.');

    return new Promise((resolve, reject) => {
      request
        .post(url)
        .send(`grant_type=refresh_token&refresh_token=${refresh_token}`)
        .end((err, res) => {
          if (err) {
            reject(err);
          }

          self.token = res.body.access_token;
          this.succeedSpinner(`Token ${self.token} atualizado com sucesso.`);

          resolve(res.body);
        });
    });
  }

  /**
   * Pega as informações do repositorio
   *
   * @public
   * @return {Promise} Retorna um objeto com os dados do repositorio
   */
  getRepository() {
    return new Promise((resolve, reject) => {
      this.startSpinner('Buscando informações do repositorio..');

      request
        .get(this.getGitUrl())
        .set('Authorization', this.getAuthorization())
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          if (err) {
            this.failSpinner('Não possivel pegar os dados do repositorio.');
            // spinner.stop();
            reject(err);

            return;
          }

          this.succeedSpinner('Dados do repositorio pego com sucesso');

          resolve(res.body);
        });
    });
  }

  /**
   * Cria um Repositorio no bitbucket
   *
   * @public
   * @return {Promise} Retorna os dados do novo repositorio
   */
  createRepository() {
    const repositoryConfig = {
      scm: 'git',
      has_wiki: false,
      fork_policy: 'no_public_forks',
      language: 'html/css',
      has_issues: true,
      is_private: true,
      description: `${this.config.repository.description}`,
      mainbranch: 'master',
    };

    this.startSpinner('Criando repositorio..');

    return new Promise((resolve, reject) => {
      request
        .post(this.getGitUrl())
        .send(repositoryConfig)
        .set('Authorization', this.getAuthorization())
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          if (err) {
            console.log(err);
            reject(err);
            this.failSpinner(`erro ao criar o repositorio: ${err}`);
          }

          this.succeedSpinner('repositorio criado com sucesso!');
          resolve(res.body);
        });
    });
  }

  /**
   * Adiciona os webhook na configuração do projeto
   *
   * @public
   * @return {Promise} Retorna os dados do webhook
   */
  webhook() {
    const url = `${this.getGitUrl()}/hooks`;

    const hooks = {
      description: 'Deploy Bot',
      url: this.config.bitbucket.webhook,
      active: true,
      events: ['repo:commit_status_updated'],
    };

    this.startSpinner('Adicionando os Webhook');

    return new Promise((resolve, reject) => {
      request
        .post(url)
        .send(hooks)
        .set('Authorization', this.getAuthorization())
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          if (err) {
            this.failSpinner('não foi possível adicionar o webhook');
            console.log(err);
            reject(err);
          }

          this.succeedSpinner('WebHooks adicionados com sucesso!');
          resolve(res.body);
        });
    });
  }

  /**
   * Ativa o pipelines no projeto
   *
   * @public
   * @return {Promise} Retorna a resposta do pipelines
   */
  activePipelines() {
    const url = `${this.getGitUrl()}/pipelines_config`;

    this.startSpinner('Ativando pipelines');

    return new Promise((resolve, reject) => {
      request
        .put(url)
        .send({ enabled: true })
        .set('Authorization', this.getAuthorization())
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          if (err) {
            console.log(err);
            this.failSpinner(`erro ao ativar o pipelines: \n${err}`);
            reject(err);
          }

          this.succeedSpinner('Pipelines ativado com sucesso!');
          resolve(res.body);
        });
    });
  }

  /**
   * Adiciona as chaves publicas e privada no pipelines
   *
   * @public
   * @return {Promise} Retorna os dados do SSH key pair do pipelines
   */
  addSSHkeys() {
    const url = `${this.getGitUrl()}/pipelines_config/ssh/key_pair`;

    this.startSpinner('Adicionado as chaves publicas e privada ao pipelines');

    return new Promise((resolve, reject) => {
      request
        .put(url)
        .send({
          private_key: this.private_key,
          public_key: this.public_key,
        })
        .set('Authorization', this.getAuthorization())
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          if (err) {
            this.failSpinner(`Erro ao adicionar o SSH: \n${err}`);
            reject(err);
          }

          this.succeedSpinner('Chaves SSH Adicionadas com sucesso!');
          resolve(res.body);
        });
    });
  }

  /**
   * Adiciona um hostname ao KnownHosts
   *
   * @public
   * @return {Promise} Retorna um objeto com os dados do hostname
   */
  addKnownHosts() {
    const url = `${this.getGitUrl()}/pipelines_config/ssh/known_hosts/`;

    this.startSpinner('Adicionado hostname ao knownHosts');

    return new Promise((resolve, reject) => {
      request
        .post(url)
        .send({
          public_key: {
            key_type: 'ssh-rsa',
            key: this.PIPELINES_PUBLIC_KEY,
          },
          hostname: this.HOSTNAME,
        })
        .set('Authorization', this.getAuthorization())
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          if (err) {
            reject(err);
          }

          this.succeedSpinner('Hostname adicionado com sucesso ao knownHosts!');
          resolve(res.body);
        });
    });
  }

  /**
   * Adiciona as variaveis de ambiente do pipelines
   *
   * @return {Promise} Returna um objeto com os dados das variaveis
   */
  addVariables() {
    const {
      config: {
        repository: {
          reponame, client_name, url_prod, url_task, project_type, segment, slugname,
        },
      },
    } = this;


    const URL_BASE = 'http://homologacao.one.com.br/';
    const FTP_PATH = '/home/one/wwwroot/hml/bradesco/portal-responsivo-20180418/';
    const stagePath = project_type === 'bradesco-portal' || project_type === 'promocao-bradesco-portal'
      ? `html/${segment}/promocoes/${slugname.toLowerCase()}`
      : slugname.toLowerCase();

    const urlStage = URL_BASE + stagePath;

    this.startSpinner('Adicionado variaveis de ambiente..');

    const secured = false;
    const variables = [
      { key: 'PIPELINES_CLIENT', value: client_name, secured },
      { key: 'PIPELINES_PROJECT_NAME', value: reponame.toLowerCase(), secured },
      { key: 'PIPELINES_URL_TASK', value: url_task, secured },
      { key: 'PIPELINES_URL_PRODUCTION', value: url_prod, secured },
      { key: 'PIPELINES_URL_HOMOLOGATION', value: urlStage, secured },
      { key: 'PROJECT_PATH', value: FTP_PATH + stagePath, secured },
    ];

    const url = `${this.getGitUrl()}/pipelines_config/variables/`;
    const requests = variables.map(variable => sendPostRequest(url, this.token, variable));

    return Promise.all(requests)
      .then((responses) => {
        this.succeedSpinner('variáveis de ambiente adicionadas com sucesso!');
        return responses;
      })
      .catch(console.error);
  }

  deleteRepository() {
    return new Promise((resolve, reject) => {
      request
        .delete(this.getGitUrl())
        .set('Authorization', this.getAuthorization())
        .end((err, res) => {
          err ? reject(err) : resolve(res.body);
        });
    });
  }
}

export default Bitbucket;
