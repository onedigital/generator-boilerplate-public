'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

var _ora = require('ora');

var _ora2 = _interopRequireDefault(_ora);

var _bitbucket = require('./bitbucket');

var _bitbucket2 = _interopRequireDefault(_bitbucket);

var _git = require('./git');

var _git2 = _interopRequireDefault(_git);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* setTimeout(() => {
spinner.text = 'Loading rainbows';
}, 1000);

setTimeout(() => {
  spinner.stop();

  //process.stdout.write('\n');
  process.exit(0);
}, 3000); */

/**
 * Boilerplate Classe
 *
 * @version 0.1.0
 * @author Roger Luiz <rogerluizm@gmail.com>
 *
 * @see create()
 * @see refreshToken()
 */


// LOCAL
var Boilerplate = function () {
  /**
   * Constructor for {@link Boilerplate}
   *
   * @constructor
   * @param {Object} options  Lista com as configurações do repositorio e do boilerplate
   */
  function Boilerplate(options) {
    (0, _classCallCheck3.default)(this, Boilerplate);

    // Inicia o loader spinner
    console.log();
    console.log('------------------------');
    console.log();

    this.spinner = (0, _ora2.default)('-');
    this.spinner.color = 'yellow';
    this.spinner.start();

    this.options = options;
    this.bitbucket = new _bitbucket2.default(this.options, this.spinner);
    this.git = new _git2.default(this.bitbucket.config, this.spinner);
  }

  /**
   * Cria o projeto no bitbucket e configura todo o projeto
   *
   * @public
   */


  (0, _createClass3.default)(Boilerplate, [{
    key: 'create',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options, bitbucket, git, spinner, _ref2, links, protocol, git_username, basedir, reponame, segment, globaldir, slugname, cloneDir, cloneURL;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                options = this.options, bitbucket = this.bitbucket, git = this.git, spinner = this.spinner;
                _context.prev = 1;
                _context.next = 4;
                return bitbucket.refreshToken();

              case 4:
                _context.next = 6;
                return bitbucket.createRepository();

              case 6:
                _ref2 = _context.sent;
                links = _ref2.links;

                bitbucket.activePipelines();

                protocol = options.protocol, git_username = options.git_username, basedir = options.basedir, reponame = options.reponame, segment = options.segment, globaldir = options.globaldir, slugname = options.slugname;
                cloneDir = _path2.default.join(basedir, reponame);
                cloneURL = protocol === 'ssh' ? links.clone[1].href : links.clone[0].href.replace('//onedigital', '//' + git_username);

                if (!segment) {
                  _context.next = 17;
                  break;
                }

                _context.next = 15;
                return git.clone(cloneURL, _path2.default.normalize(cloneDir), _path2.default.normalize(globaldir), [slugname, segment]);

              case 15:
                _context.next = 19;
                break;

              case 17:
                _context.next = 19;
                return git.clone(cloneURL, _path2.default.normalize(cloneDir), _path2.default.normalize(globaldir));

              case 19:
                _context.next = 21;
                return Promise.all([bitbucket.webhook(), bitbucket.addSSHkeys(), bitbucket.addKnownHosts(), bitbucket.addVariables()]);

              case 21:

                spinner.start('Finalizando..');
                spinner.succeed('Projeto gerado com sucesso :)');

                console.log();
                console.log('------------------------');
                console.log();

                return _context.abrupt('return', {
                  success: true
                });

              case 29:
                _context.prev = 29;
                _context.t0 = _context['catch'](1);

                console.log(_context.t0);
                spinner.fail('ocorreu um erro: ' + _context.t0);
                return _context.abrupt('return', _context.t0);

              case 34:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 29]]);
      }));

      function create() {
        return _ref.apply(this, arguments);
      }

      return create;
    }()

    /**
     * Gera um novo token de acesso
     *
     * @public
     */

  }, {
    key: 'refreshToken',
    value: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
        var bitbucket, _ref4, access_token;

        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                bitbucket = this.bitbucket;
                _context2.next = 3;
                return bitbucket.refreshToken();

              case 3:
                _ref4 = _context2.sent;
                access_token = _ref4.access_token;


                console.log('' + _chalk2.default.green('Token atualizado com sucesso access_token: \n'));
                console.log('' + _chalk2.default.blue(access_token));

              case 7:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function refreshToken() {
        return _ref3.apply(this, arguments);
      }

      return refreshToken;
    }()
  }]);
  return Boilerplate;
}();

module.exports = function (options) {
  return new Boilerplate(options);
};