'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _execa = require('execa');

var _execa2 = _interopRequireDefault(_execa);

var _execSeries = require('exec-series');

var _execSeries2 = _interopRequireDefault(_execSeries);

var _fsExtra = require('fs-extra');

var _fsExtra2 = _interopRequireDefault(_fsExtra);

var _client = require('./client');

var _client2 = _interopRequireDefault(_client);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Lista de metodos para ajudar a criar,
 * clonar e fazer commits usando a API do Bitbucket
 *
 * @version 0.1.0
 * @author Roger Luiz <rogerluizm@gmail.com>
 *
 * @see clone()
 */
var Git = function () {
  /**
   * Constructor for {@link Bitbucket}
   *
   * @constructor
   * @param {Object} options Lista com as configurações do repositorio e do boilerplate
   */
  function Git(config, spinner) {
    (0, _classCallCheck3.default)(this, Git);

    this.config = config;
    this.spinner = spinner;
  }

  /**
   * Clona o repositorio criando, copia os arquivos para a pasta
   * onde o usuario esta, faz o commit criar as branchs
   *
   * @public
   * @param {string} url A url do projeto criado
   * @param {string} dest A pasta onde o projeto vai ser clonado
   * @param {string} from Caminho da onde o templete é copiado
   * @param {Object} changeVarNames Variaveis a ser alteradas
   * em casa arquivo e paginas quando o template for do portal
   * @example clone('url-git', './', 'templates/template-escolhido',
   *  ['project-name', 'category-name'])
   */


  (0, _createClass3.default)(Git, [{
    key: 'clone',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(url, dest, from, changeVarNames) {
        var pathFrom, whatTemplate, client;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                // const { commit, cloneTemplate, createBranchs, config, spinner } = this;
                pathFrom = _path2.default.join(from, 'template');
                whatTemplate = this.config.repository.project_type;
                client = new _client2.default(this.config, this.spinner);


                this.spinner.start('Clonado repositorio do projeto..');
                _context.next = 7;
                return _execa2.default.shell('git clone ' + url + ' ' + dest + ' -q');

              case 7:
                this.spinner.succeed('Repositorio clonado com sucesso');

                _context.next = 10;
                return this.cloneTemplate(whatTemplate, this.config);

              case 10:
                _context.next = 12;
                return client.copy(pathFrom, dest);

              case 12:
                _context.next = 14;
                return client.remove(pathFrom);

              case 14:
                if (!(whatTemplate === 'interno')) {
                  _context.next = 17;
                  break;
                }

                _context.next = 17;
                return this.useFrameworkCli(this.config.repository.framework, dest);

              case 17:
                if (!(whatTemplate === 'bradesco-portal')) {
                  _context.next = 22;
                  break;
                }

                _context.next = 20;
                return client.renameVaribalesInFiles(dest, [/project-name/g, /category-name/g], changeVarNames);

              case 20:
                _context.next = 22;
                return client.rename(dest, ['project-name', 'category-name'], changeVarNames);

              case 22:
                if (!(whatTemplate === 'promocao-bradesco-portal')) {
                  _context.next = 27;
                  break;
                }

                _context.next = 25;
                return client.renameVaribalesInFiles(dest, [/project-name/g, /category-name/g], changeVarNames);

              case 25:
                _context.next = 27;
                return client.rename(dest, ['project-name', 'category-name'], changeVarNames);

              case 27:
                _context.next = 29;
                return this.commit(dest);

              case 29:
                _context.next = 31;
                return this.createBranchs(dest);

              case 31:
                return _context.abrupt('return', {
                  success: true
                });

              case 34:
                _context.prev = 34;
                _context.t0 = _context['catch'](0);

                console.error('Alguma coisa deu errada: ' + _context.t0);
                return _context.abrupt('return', _context.t0);

              case 38:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 34]]);
      }));

      function clone(_x, _x2, _x3, _x4) {
        return _ref.apply(this, arguments);
      }

      return clone;
    }()

    /**
     * Clona o template escolhido dentro do projeto
     *
     * @public
     * @param {string} template Nome do template escolhido
     * @param {Object} config Configuração do repositorio
     */

  }, {
    key: 'cloneTemplate',
    value: function () {
      var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(template) {
        var _config$repository, git_username, globaldir, dest, url, _ref3, stdout;

        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _config$repository = this.config.repository, git_username = _config$repository.git_username, globaldir = _config$repository.globaldir;
                dest = _path2.default.normalize(_path2.default.join(globaldir, 'template'));
                url = 'https://' + git_username + '@bitbucket.org/onedigital/one-templates-' + template + '.git';


                this.spinner.start('Clonando template..');
                _context2.next = 7;
                return _execa2.default.shell('git clone ' + url + ' ' + dest + ' -q');

              case 7:
                _ref3 = _context2.sent;
                stdout = _ref3.stdout;

                this.spinner.succeed('Template clonado com sucesso!');

                return _context2.abrupt('return', stdout);

              case 13:
                _context2.prev = 13;
                _context2.t0 = _context2['catch'](0);

                console.error('Erro ao clonar o template: ' + _context2.t0);

                return _context2.abrupt('return', _context2.t0);

              case 17:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 13]]);
      }));

      function cloneTemplate(_x5) {
        return _ref2.apply(this, arguments);
      }

      return cloneTemplate;
    }()

    /**
     * Caso seja um projeto interno, baixa o framework escolhido
     *
     * @public
     * @param {string} framework Nome do framework escolhido 
     * @param {string} dest Diretorio do projeto criado
     */

  }, {
    key: 'useFrameworkCli',
    value: function () {
      var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(framework, dest) {
        var client, inlinePreset, _ref5, stdout, pathFrom, pathFromReadme;

        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                client = new _client2.default(this.config, this.spinner);

                this.spinner.start('Baixando o ' + framework + ' e depend\xEAncias...');

                if (!(framework === 'vue')) {
                  _context3.next = 19;
                  break;
                }

                inlinePreset = "{\\\"useConfigFiles\\\":true,\\\"plugins\\\":{\\\"@vue\\\/cli-plugin-babel\\\":{},\\\"@vue\\\/cli-plugin-pwa\\\":{},\\\"@vue\\\/cli-plugin-router\\\":{\\\"historyMode\\\":true},\\\"@vue\\\/cli-plugin-vuex\\\":{},\\\"@vue\\\/cli-plugin-eslint\\\":{\\\"config\\\":\\\"standard\\\",\\\"lintOn\\\":[\\\"save\\\"]},\\\"@vue\\\/cli-plugin-unit-jest\\\":{}},\\\"cssPreprocessor\\\":\\\"node-sass\\\"}";
                _context3.next = 7;
                return _execa2.default.shell('cd ' + dest + ' && vue create client --skipGetStarted --inlinePreset ' + inlinePreset);

              case 7:
                _ref5 = _context3.sent;
                stdout = _ref5.stdout;
                pathFrom = _path2.default.join(dest, 'client');
                pathFromReadme = _path2.default.join(pathFrom, 'README.md');
                _context3.next = 13;
                return client.remove(pathFromReadme);

              case 13:
                _context3.next = 15;
                return client.copy(pathFrom, dest);

              case 15:
                _context3.next = 17;
                return client.remove(pathFrom);

              case 17:
                this.spinner.succeed('Framework baixado com sucesso!');
                return _context3.abrupt('return', stdout);

              case 19:
                return _context3.abrupt('return', true);

              case 22:
                _context3.prev = 22;
                _context3.t0 = _context3['catch'](0);

                console.error('Erro ao usar o cli do framework ' + framework + ': ' + _context3.t0);

                return _context3.abrupt('return', _context3.t0);

              case 26:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 22]]);
      }));

      function useFrameworkCli(_x6, _x7) {
        return _ref4.apply(this, arguments);
      }

      return useFrameworkCli;
    }()

    /**
     * Faz o primeiro commit do projeto
     *
     * @public
     * @method commit
     * @param {string} dest Diretorio do projeto criado
     * @return {Promise} Retorna uma promise logo apos o commit
     */

  }, {
    key: 'commit',
    value: function () {
      var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(dest) {
        var spinner, commit, branch, _ref7, stdouts;

        return _regenerator2.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                spinner = this.spinner;
                commit = 'Adicionando a estrutura do projeto [skip ci]';
                branch = 'master';
                _context4.prev = 3;

                spinner.start('Enviado commit..');

                _context4.next = 7;
                return _execa2.default.shell('cd ' + dest + ' && git add . && git commit -am "' + commit + '" && git push origin ' + branch);

              case 7:
                _ref7 = _context4.sent;
                stdouts = _ref7.stdouts;

                spinner.succeed('Commit efetuado com sucesso');

                return _context4.abrupt('return', stdouts);

              case 13:
                _context4.prev = 13;
                _context4.t0 = _context4['catch'](3);

                console.error(_context4.t0);

                return _context4.abrupt('return', _context4.t0);

              case 17:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee4, this, [[3, 13]]);
      }));

      function commit(_x8) {
        return _ref6.apply(this, arguments);
      }

      return commit;
    }()

    /**
     * Cria as branches do projeto seguindo o git flow
     *
     * @public
     * @param {string} dest Diretorio do projeto criado
     * @return {Promise} Retorna uma promise logo apos o criar as branches
     */

  }, {
    key: 'createBranchs',
    value: function createBranchs(dest) {
      var spinner = this.spinner;


      spinner.start('Criando as branchs do repositorio..');

      return new Promise(function (resolve, reject) {
        (0, _execSeries2.default)(['cd ' + dest + ' && git checkout -b stage && git push origin stage', 'cd ' + dest + ' && git checkout -b develop && git push origin develop'], function (err, stdouts) {
          if (err) {
            spinner.fail('erro na hora de criar as braches');

            reject(err);
            return;
          }

          spinner.succeed('Branchs criadas com sucesso!');

          resolve(stdouts);
        });
      });
    }
  }]);
  return Git;
}();

// LOCAL


exports.default = Git;