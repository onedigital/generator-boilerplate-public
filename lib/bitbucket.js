'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _superagent = require('superagent');

var _superagent2 = _interopRequireDefault(_superagent);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Bitbucket utils
 *
 * @version 0.1.0
 * @author Roger Luiz <rogerluizm@gmail.com>
 *
 * @see getToken()
 * @see refreshToken()
 * @see getRepository()
 * @see createRepository()
 * @see webhook()
 * @see activePipelines()
 * @see addSSHkeys()
 * @see addKnownHosts()
 * @see addVariables()
 */
var Bitbucket = function () {
  /**
   * Constructor for {@link Bitbucket}
   *
   * @constructor
   * @param {Object} options Lista com as configurações do repositorio e do boilerplate
   */
  function Bitbucket() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var spinner = arguments[1];
    (0, _classCallCheck3.default)(this, Bitbucket);

    this.bitbucket_url = 'https://api.bitbucket.org/2.0/repositories';
    this.token = '';
    this.spinner = spinner;
    this.config = { bitbucket: Object.assign(options.BITBUCKET_KEYS, options.bitbucket) };
    this.PIPELINES_PUBLIC_KEY = options.PIPELINES_PUBLIC_KEY;
    this.HOSTNAME = options.HOSTNAME;
    this.config.repository = options;
    this.private_key = options.private_key;
    this.public_key = options.public_key;
  }

  (0, _createClass3.default)(Bitbucket, [{
    key: 'doActionWithSpinner',
    value: function doActionWithSpinner(text, action) {
      if ((0, _utils.isTest)()) this.spinner[action](text);
    }
  }, {
    key: 'startSpinner',
    value: function startSpinner(text) {
      this.doActionWithSpinner(text, 'start');
    }
  }, {
    key: 'succeedSpinner',
    value: function succeedSpinner(text) {
      this.doActionWithSpinner(text, 'succeed');
    }
  }, {
    key: 'failSpinner',
    value: function failSpinner(text) {
      this.doActionWithSpinner(text, 'fail');
    }
  }, {
    key: 'getAuthorization',
    value: function getAuthorization() {
      return 'Bearer ' + this.token;
    }
  }, {
    key: 'getGitUrl',
    value: function getGitUrl() {
      return this.bitbucket_url + '/' + this.config.bitbucket.username + '/' + this.config.repository.reponame.toLowerCase();
    }

    /**
     * Gera um novo token do bitbucket
     *
     * @public
     * @return {Promise} Retorna uma promise com o resultado do token
     */

  }, {
    key: 'getToken',
    value: function getToken() {
      var _this = this;

      var self = this;
      var _config$bitbucket = this.config.bitbucket,
          client_id = _config$bitbucket.client_id,
          secret = _config$bitbucket.secret,
          grant_type = _config$bitbucket.grant_type;

      var url = 'https://' + client_id + ':' + secret + '@bitbucket.org/site/oauth2/access_token';

      this.startSpinner('Gerando um novo token.');

      return new Promise(function (resolve, reject) {
        _superagent2.default.post(url).send('grant_type=' + grant_type).end(function (err, res) {
          if (err) {
            _this.failSpinner('Nao foi prossivel gerar o token');

            reject(err);
          }

          self.token = res.body.access_token;
          _this.succeedSpinner('Token ' + self.token + ' gerado com sucesso.');

          resolve(res.body);
        });
      });
    }

    /**
     * Faz o refresh do token do bitbucket
     *
     * @public
     * @return {Promise} Retorna uma promise com o resultado do token
     */

  }, {
    key: 'refreshToken',
    value: function refreshToken() {
      var _this2 = this;

      var self = this;
      var _config$bitbucket2 = this.config.bitbucket,
          client_id = _config$bitbucket2.client_id,
          secret = _config$bitbucket2.secret,
          refresh_token = _config$bitbucket2.refresh_token;

      var url = 'https://' + client_id + ':' + secret + '@bitbucket.org/site/oauth2/access_token';

      this.startSpinner('Atualizado token gerando.');

      return new Promise(function (resolve, reject) {
        _superagent2.default.post(url).send('grant_type=refresh_token&refresh_token=' + refresh_token).end(function (err, res) {
          if (err) {
            reject(err);
          }

          self.token = res.body.access_token;
          _this2.succeedSpinner('Token ' + self.token + ' atualizado com sucesso.');

          resolve(res.body);
        });
      });
    }

    /**
     * Pega as informações do repositorio
     *
     * @public
     * @return {Promise} Retorna um objeto com os dados do repositorio
     */

  }, {
    key: 'getRepository',
    value: function getRepository() {
      var _this3 = this;

      return new Promise(function (resolve, reject) {
        _this3.startSpinner('Buscando informações do repositorio..');

        _superagent2.default.get(_this3.getGitUrl()).set('Authorization', _this3.getAuthorization()).set('Content-Type', 'application/json').end(function (err, res) {
          if (err) {
            _this3.failSpinner('Não possivel pegar os dados do repositorio.');
            // spinner.stop();
            reject(err);

            return;
          }

          _this3.succeedSpinner('Dados do repositorio pego com sucesso');

          resolve(res.body);
        });
      });
    }

    /**
     * Cria um Repositorio no bitbucket
     *
     * @public
     * @return {Promise} Retorna os dados do novo repositorio
     */

  }, {
    key: 'createRepository',
    value: function createRepository() {
      var _this4 = this;

      var repositoryConfig = {
        scm: 'git',
        has_wiki: false,
        fork_policy: 'no_public_forks',
        language: 'html/css',
        has_issues: true,
        is_private: true,
        description: '' + this.config.repository.description,
        mainbranch: 'master'
      };

      this.startSpinner('Criando repositorio..');

      return new Promise(function (resolve, reject) {
        _superagent2.default.post(_this4.getGitUrl()).send(repositoryConfig).set('Authorization', _this4.getAuthorization()).set('Content-Type', 'application/json').end(function (err, res) {
          if (err) {
            console.log(err);
            reject(err);
            _this4.failSpinner('erro ao criar o repositorio: ' + err);
          }

          _this4.succeedSpinner('repositorio criado com sucesso!');
          resolve(res.body);
        });
      });
    }

    /**
     * Adiciona os webhook na configuração do projeto
     *
     * @public
     * @return {Promise} Retorna os dados do webhook
     */

  }, {
    key: 'webhook',
    value: function webhook() {
      var _this5 = this;

      var url = this.getGitUrl() + '/hooks';

      var hooks = {
        description: 'Deploy Bot',
        url: this.config.bitbucket.webhook,
        active: true,
        events: ['repo:commit_status_updated']
      };

      this.startSpinner('Adicionando os Webhook');

      return new Promise(function (resolve, reject) {
        _superagent2.default.post(url).send(hooks).set('Authorization', _this5.getAuthorization()).set('Content-Type', 'application/json').end(function (err, res) {
          if (err) {
            _this5.failSpinner('não foi possível adicionar o webhook');
            console.log(err);
            reject(err);
          }

          _this5.succeedSpinner('WebHooks adicionados com sucesso!');
          resolve(res.body);
        });
      });
    }

    /**
     * Ativa o pipelines no projeto
     *
     * @public
     * @return {Promise} Retorna a resposta do pipelines
     */

  }, {
    key: 'activePipelines',
    value: function activePipelines() {
      var _this6 = this;

      var url = this.getGitUrl() + '/pipelines_config';

      this.startSpinner('Ativando pipelines');

      return new Promise(function (resolve, reject) {
        _superagent2.default.put(url).send({ enabled: true }).set('Authorization', _this6.getAuthorization()).set('Content-Type', 'application/json').end(function (err, res) {
          if (err) {
            console.log(err);
            _this6.failSpinner('erro ao ativar o pipelines: \n' + err);
            reject(err);
          }

          _this6.succeedSpinner('Pipelines ativado com sucesso!');
          resolve(res.body);
        });
      });
    }

    /**
     * Adiciona as chaves publicas e privada no pipelines
     *
     * @public
     * @return {Promise} Retorna os dados do SSH key pair do pipelines
     */

  }, {
    key: 'addSSHkeys',
    value: function addSSHkeys() {
      var _this7 = this;

      var url = this.getGitUrl() + '/pipelines_config/ssh/key_pair';

      this.startSpinner('Adicionado as chaves publicas e privada ao pipelines');

      return new Promise(function (resolve, reject) {
        _superagent2.default.put(url).send({
          private_key: _this7.private_key,
          public_key: _this7.public_key
        }).set('Authorization', _this7.getAuthorization()).set('Content-Type', 'application/json').end(function (err, res) {
          if (err) {
            _this7.failSpinner('Erro ao adicionar o SSH: \n' + err);
            reject(err);
          }

          _this7.succeedSpinner('Chaves SSH Adicionadas com sucesso!');
          resolve(res.body);
        });
      });
    }

    /**
     * Adiciona um hostname ao KnownHosts
     *
     * @public
     * @return {Promise} Retorna um objeto com os dados do hostname
     */

  }, {
    key: 'addKnownHosts',
    value: function addKnownHosts() {
      var _this8 = this;

      var url = this.getGitUrl() + '/pipelines_config/ssh/known_hosts/';

      this.startSpinner('Adicionado hostname ao knownHosts');

      return new Promise(function (resolve, reject) {
        _superagent2.default.post(url).send({
          public_key: {
            key_type: 'ssh-rsa',
            key: _this8.PIPELINES_PUBLIC_KEY
          },
          hostname: _this8.HOSTNAME
        }).set('Authorization', _this8.getAuthorization()).set('Content-Type', 'application/json').end(function (err, res) {
          if (err) {
            reject(err);
          }

          _this8.succeedSpinner('Hostname adicionado com sucesso ao knownHosts!');
          resolve(res.body);
        });
      });
    }

    /**
     * Adiciona as variaveis de ambiente do pipelines
     *
     * @return {Promise} Returna um objeto com os dados das variaveis
     */

  }, {
    key: 'addVariables',
    value: function addVariables() {
      var _this9 = this;

      var _config$repository = this.config.repository,
          reponame = _config$repository.reponame,
          client_name = _config$repository.client_name,
          url_prod = _config$repository.url_prod,
          url_task = _config$repository.url_task,
          project_type = _config$repository.project_type,
          segment = _config$repository.segment,
          slugname = _config$repository.slugname;


      var URL_BASE = 'http://homologacao.one.com.br/';
      var FTP_PATH = '/home/one/wwwroot/hml/bradesco/portal-responsivo-20180418/';
      var stagePath = project_type === 'bradesco-portal' || project_type === 'promocao-bradesco-portal' ? 'html/' + segment + '/promocoes/' + slugname.toLowerCase() : slugname.toLowerCase();

      var urlStage = URL_BASE + stagePath;

      this.startSpinner('Adicionado variaveis de ambiente..');

      var secured = false;
      var variables = [{ key: 'PIPELINES_CLIENT', value: client_name, secured: secured }, { key: 'PIPELINES_PROJECT_NAME', value: reponame.toLowerCase(), secured: secured }, { key: 'PIPELINES_URL_TASK', value: url_task, secured: secured }, { key: 'PIPELINES_URL_PRODUCTION', value: url_prod, secured: secured }, { key: 'PIPELINES_URL_HOMOLOGATION', value: urlStage, secured: secured }, { key: 'PROJECT_PATH', value: FTP_PATH + stagePath, secured: secured }];

      var url = this.getGitUrl() + '/pipelines_config/variables/';
      var requests = variables.map(function (variable) {
        return (0, _utils.sendPostRequest)(url, _this9.token, variable);
      });

      return Promise.all(requests).then(function (responses) {
        _this9.succeedSpinner('variáveis de ambiente adicionadas com sucesso!');
        return responses;
      }).catch(console.error);
    }
  }, {
    key: 'deleteRepository',
    value: function deleteRepository() {
      var _this10 = this;

      return new Promise(function (resolve, reject) {
        _superagent2.default.delete(_this10.getGitUrl()).set('Authorization', _this10.getAuthorization()).end(function (err, res) {
          err ? reject(err) : resolve(res.body);
        });
      });
    }
  }]);
  return Bitbucket;
}();

exports.default = Bitbucket;