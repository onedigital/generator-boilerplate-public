import fs from 'fs-extra';
import path from 'path';
import execa from 'execa';
import replaceInFile from 'replace-in-file';
import {
  normalizePath
} from './utils';


/**
 * Client utils
 *
 * @author Roger Luiz <rogerluizm@gmail.com>
 * @since 1.0.0
 */
class Client {
  /**
   * Constructor for {@link Client}
   *
   * @constructor
   * @param {Object} config Lista com as configurações do repositorio e do boilerplate
   */
  constructor(config, spinner) {
    this.config = config;
    this.spinner = spinner;
  }

  /**
   * Renomeia pastas com uma sequencia de variaveis
   *
   * @param {string} dir Diretorio base para procurar os arquivos e fazer a modificação
   * @param {string} from String a ser trocada
   * @param {string} to Nova string para ser adicionada
   * @param {function} callback Callback para chamar com rename
   */
  async rename(dir, from, to) {
    const {
      spinner
    } = this;
    const ROOT_DIR = normalizePath(`${dir}/**/*`);
    const FIND_STR = from;
    const REPLACE_STR = to;

    spinner.start('Renomeando os arquivos..');

    try {
      if (typeof REPLACE_STR === 'string') {
        await execa.shell(`renamer --find ${FIND_STR} --replace ${REPLACE_STR} ${ROOT_DIR}`);
      } else {
        await execa.shell(`renamer --find ${FIND_STR[0]} --replace ${REPLACE_STR[0]} ${ROOT_DIR} && renamer --find ${FIND_STR[1]} --replace ${REPLACE_STR[1]} ${ROOT_DIR}`);
      }

      spinner.succeed('Todos os arquivos e diretorios modificados');

      return {
        success: true
      };
    } catch (err) {
      console.error(err);

      return err;
    }
  }

  /**
   * Copia os aquivos de uma pasta para outra
   *
   * @param {string} from Local de origem dos arquivos
   * @param {string} to Local para onde os arquivos vão ser copiados
   */
  async copy(from, to) {
    const {
      spinner
    } = this;

    spinner.start('Copiando arquivos..');

    try {
      await fs.copy(from, to, {
        overwrite: true,
        filter: n =>
          // copia todos os arquivos menos o .git
          (n !== path.join(from, '.git')),
      });

      spinner.succeed('Arquivos copiados com sucesso!');

      return {
        success: true
      };
    } catch (err) {
      console.error(err);

      return err;
    }
  }

  /**
   * Remove os arquivos
   *
   * @param {string} dir Local para deletar os arquivos
   */
  async remove(dir) {
    const {
      spinner
    } = this;

    spinner.start('Deletando arquivos temporarios..');

    try {
      await fs.remove(dir);
      spinner.succeed(`${dir} deletado com sucesso!`);

      return {
        success: true
      };
    } catch (err) {
      console.error(err);

      return err;
    }
  }

  /**
   * Renomeia string dentro dos arquivos
   *
   * @param {string} dir Diretorio onde os arquivos estão
   * @param {string} from String a ser trocada
   * @param {string} to Nova string para ser adicionada
   */
  async renameVaribalesInFiles(dir, from, to) {
    const {
      spinner
    } = this;
    const options = {
      files: normalizePath(`${dir}/**/*`),
      from,
      to,
    };

    spinner.start('Renomeando as variveis nos arquivos..');

    try {
      // muda as variaveis nos arquivos
      await replaceInFile(options);

      spinner.succeed('Todas vas variaveis modificadas nos arquivos');

      return {
        success: true
      };
    } catch (error) {
      console.error('Error occurred:', error);

      return error;
    }
  }
}
export default Client;
