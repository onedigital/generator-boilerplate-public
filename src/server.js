const express = require('express');
const app = express();

app.get('/', (req, res)=> {
  // res.send('Hello Dev!');
  res.json(res.body);
});

app.get('/auth', (req, res) => {
  res.json(res.body);
});

app.listen(4000, () => {
  console.log('Dev app listening on port 4000!');
});