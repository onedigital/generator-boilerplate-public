'use strict';

var express = require('express');
var app = express();

app.get('/', function (req, res) {
  // res.send('Hello Dev!');
  res.json(res.body);
});

app.get('/auth', function (req, res) {
  res.json(res.body);
});

app.listen(4000, function () {
  console.log('Dev app listening on port 4000!');
});