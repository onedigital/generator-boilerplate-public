// import Boilerplate from '../src';
import ora from 'ora';
import Bitbucket from '../src/bitbucket';
import { findVariablePerKey } from '../src/utils';
import { HOSTNAME, TESTE_KEYS } from '../src/constants';

const spinner = ora('-');
spinner.color = 'yellow';

jest.setTimeout(10000);

describe('BitBucket', () => {
  const options = {
    reponame: 'teste',
    slugname: 'teste-boilerplate',
    git_username: 'deploy_one',
    url_task: 'https://jobs.one.com.br/',
    url_prod: 'https://banco.bradesco/',
    client_name: 'bradesco_car',
    project_type: 'simple',
    protocol: 'https',
    basedir: 'C:\\Sites',
    dest: 'C:\\Sites\\12',
    description: 'Gerador de Boilerplate',
    bitbucket: TESTE_KEYS,
  };
  const bitbucket = new Bitbucket(options, spinner);

  beforeAll(async () => {
    try {
      await bitbucket.refreshToken();
      await bitbucket.createRepository();
    } catch (error) {
      // console.log(error);
    }
  });

  afterAll(async () => {
    try {
      await bitbucket.deleteRepository();
    } catch (error) {
      // console.log(error);
    }
  });

  describe('refreshToken', () => {
    it('should return a new token', async () => {
      const body = await bitbucket.refreshToken();
      expect(body.access_token).toBeDefined();
      expect(body.access_token).toBe(bitbucket.token);
    });
  });

  describe('webHook', () => {
    it('it should add a webhook', async () => {
      const body = await bitbucket.webhook();
      expect(body.description).toBe('Deploy Bot');
      expect(body.url).toBe(options.bitbucket.webhook);
      expect(body.active).toBe(true);
    });
  });

  describe('activePipelines', () => {
    it('it should active the pipelines', async () => {
      const body = await bitbucket.activePipelines();
      expect(body.enabled).toBe(true);
    });
  });

  describe('addSSHkeys', () => {
    it('it should add the ssh keys', async () => {
      const body = await bitbucket.addSSHkeys();
      expect(body.public_key).toContain('ssh-rsa');
    });
  });

  describe('addKnownHosts', () => {
    it('it should add the knows hosts', async () => {
      const body = await bitbucket.addKnownHosts();
      expect(body.hostname).toBe(HOSTNAME);
    });
  });

  describe('addVariables', () => {
    it('it should add the variables', async () => {
      const body = await bitbucket.addVariables();
      expect(body.find(variable => findVariablePerKey(variable, 'PIPELINES_CLIENT'))).toBeDefined();
      expect(body.find(variable => findVariablePerKey(variable, 'PIPELINES_PROJECT_NAME'))).toBeDefined();
      expect(body.find(variable => findVariablePerKey(variable, 'PIPELINES_URL_TASK'))).toBeDefined();
      expect(body.find(variable => findVariablePerKey(variable, 'PIPELINES_URL_PRODUCTION'))).toBeDefined();
      expect(body.find(variable => findVariablePerKey(variable, 'PIPELINES_URL_HOMOLOGATION'))).toBeDefined();
    });
  });
});
