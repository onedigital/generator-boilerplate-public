'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _fsExtra = require('fs-extra');

var _fsExtra2 = _interopRequireDefault(_fsExtra);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _execa = require('execa');

var _execa2 = _interopRequireDefault(_execa);

var _replaceInFile = require('replace-in-file');

var _replaceInFile2 = _interopRequireDefault(_replaceInFile);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Client utils
 *
 * @author Roger Luiz <rogerluizm@gmail.com>
 * @since 1.0.0
 */
var Client = function () {
  /**
   * Constructor for {@link Client}
   *
   * @constructor
   * @param {Object} config Lista com as configurações do repositorio e do boilerplate
   */
  function Client(config, spinner) {
    (0, _classCallCheck3.default)(this, Client);

    this.config = config;
    this.spinner = spinner;
  }

  /**
   * Renomeia pastas com uma sequencia de variaveis
   *
   * @param {string} dir Diretorio base para procurar os arquivos e fazer a modificação
   * @param {string} from String a ser trocada
   * @param {string} to Nova string para ser adicionada
   * @param {function} callback Callback para chamar com rename
   */


  (0, _createClass3.default)(Client, [{
    key: 'rename',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(dir, from, to) {
        var spinner, ROOT_DIR, FIND_STR, REPLACE_STR;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                spinner = this.spinner;
                ROOT_DIR = (0, _utils.normalizePath)(dir + '/**/*');
                FIND_STR = from;
                REPLACE_STR = to;


                spinner.start('Renomeando os arquivos..');

                _context.prev = 5;

                if (!(typeof REPLACE_STR === 'string')) {
                  _context.next = 11;
                  break;
                }

                _context.next = 9;
                return _execa2.default.shell('renamer --find ' + FIND_STR + ' --replace ' + REPLACE_STR + ' ' + ROOT_DIR);

              case 9:
                _context.next = 13;
                break;

              case 11:
                _context.next = 13;
                return _execa2.default.shell('renamer --find ' + FIND_STR[0] + ' --replace ' + REPLACE_STR[0] + ' ' + ROOT_DIR + ' && renamer --find ' + FIND_STR[1] + ' --replace ' + REPLACE_STR[1] + ' ' + ROOT_DIR);

              case 13:

                spinner.succeed('Todos os arquivos e diretorios modificados');

                return _context.abrupt('return', {
                  success: true
                });

              case 17:
                _context.prev = 17;
                _context.t0 = _context['catch'](5);

                console.error(_context.t0);

                return _context.abrupt('return', _context.t0);

              case 21:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[5, 17]]);
      }));

      function rename(_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      }

      return rename;
    }()

    /**
     * Copia os aquivos de uma pasta para outra
     *
     * @param {string} from Local de origem dos arquivos
     * @param {string} to Local para onde os arquivos vão ser copiados
     */

  }, {
    key: 'copy',
    value: function () {
      var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(from, to) {
        var spinner;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                spinner = this.spinner;


                spinner.start('Copiando arquivos..');

                _context2.prev = 2;
                _context2.next = 5;
                return _fsExtra2.default.copy(from, to, {
                  overwrite: true,
                  filter: function filter(n) {
                    return (
                      // copia todos os arquivos menos o .git
                      n !== _path2.default.join(from, '.git')
                    );
                  }
                });

              case 5:

                spinner.succeed('Arquivos copiados com sucesso!');

                return _context2.abrupt('return', {
                  success: true
                });

              case 9:
                _context2.prev = 9;
                _context2.t0 = _context2['catch'](2);

                console.error(_context2.t0);

                return _context2.abrupt('return', _context2.t0);

              case 13:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this, [[2, 9]]);
      }));

      function copy(_x4, _x5) {
        return _ref2.apply(this, arguments);
      }

      return copy;
    }()

    /**
     * Remove os arquivos
     *
     * @param {string} dir Local para deletar os arquivos
     */

  }, {
    key: 'remove',
    value: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(dir) {
        var spinner;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                spinner = this.spinner;


                spinner.start('Deletando arquivos temporarios..');

                _context3.prev = 2;
                _context3.next = 5;
                return _fsExtra2.default.remove(dir);

              case 5:
                spinner.succeed(dir + ' deletado com sucesso!');

                return _context3.abrupt('return', {
                  success: true
                });

              case 9:
                _context3.prev = 9;
                _context3.t0 = _context3['catch'](2);

                console.error(_context3.t0);

                return _context3.abrupt('return', _context3.t0);

              case 13:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this, [[2, 9]]);
      }));

      function remove(_x6) {
        return _ref3.apply(this, arguments);
      }

      return remove;
    }()

    /**
     * Renomeia string dentro dos arquivos
     *
     * @param {string} dir Diretorio onde os arquivos estão
     * @param {string} from String a ser trocada
     * @param {string} to Nova string para ser adicionada
     */

  }, {
    key: 'renameVaribalesInFiles',
    value: function () {
      var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(dir, from, to) {
        var spinner, options;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                spinner = this.spinner;
                options = {
                  files: (0, _utils.normalizePath)(dir + '/**/*'),
                  from: from,
                  to: to
                };


                spinner.start('Renomeando as variveis nos arquivos..');

                _context4.prev = 3;
                _context4.next = 6;
                return (0, _replaceInFile2.default)(options);

              case 6:

                spinner.succeed('Todas vas variaveis modificadas nos arquivos');

                return _context4.abrupt('return', {
                  success: true
                });

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4['catch'](3);

                console.error('Error occurred:', _context4.t0);

                return _context4.abrupt('return', _context4.t0);

              case 14:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee4, this, [[3, 10]]);
      }));

      function renameVaribalesInFiles(_x7, _x8, _x9) {
        return _ref4.apply(this, arguments);
      }

      return renameVaribalesInFiles;
    }()
  }]);
  return Client;
}();

exports.default = Client;