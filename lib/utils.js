'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.normalizePath = exports.readKey = exports.findVariablePerKey = exports.sendPostRequest = exports.isTest = undefined;

var _superagent = require('superagent');

var _superagent2 = _interopRequireDefault(_superagent);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isTest = exports.isTest = function isTest() {
  return process.env.NODE_ENV !== 'test';
};

var sendPostRequest = exports.sendPostRequest = function sendPostRequest(url, token, variable) {
  return new Promise(function (resolve, reject) {
    _superagent2.default.post(url).send(JSON.stringify(variable)).set('Authorization', 'Bearer ' + token).set('Content-Type', 'application/json').end(function (err, res) {
      err ? reject(err) : resolve(res.body);
    });
  });
};

var findVariablePerKey = exports.findVariablePerKey = function findVariablePerKey(variable, key) {
  return variable.key === key;
};

var readKey = exports.readKey = function readKey(keyName) {
  return _fs2.default.readFileSync(_path2.default.resolve(__dirname, '../' + keyName)).toString();
};

/**
* Normaliza um caminho usando path.normalize, e remove as barras duplas
*
* @param {string} str Caminho a ser normalizado
*/
var normalizePath = exports.normalizePath = function normalizePath(str) {
  return _path2.default.normalize(str).replace(/\\+/g, '/');
};