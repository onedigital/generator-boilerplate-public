import path from 'path';
import execa from 'execa';
import execSeries from 'exec-series';
import fs from 'fs-extra';

// LOCAL
import Client from './client';

/**
 * Lista de metodos para ajudar a criar,
 * clonar e fazer commits usando a API do Bitbucket
 *
 * @version 0.1.0
 * @author Roger Luiz <rogerluizm@gmail.com>
 *
 * @see clone()
 */
class Git {
  /**
   * Constructor for {@link Bitbucket}
   *
   * @constructor
   * @param {Object} options Lista com as configurações do repositorio e do boilerplate
   */
  constructor(config, spinner) {
    this.config = config;
    this.spinner = spinner;
  }

  /**
   * Clona o repositorio criando, copia os arquivos para a pasta
   * onde o usuario esta, faz o commit criar as branchs
   *
   * @public
   * @param {string} url A url do projeto criado
   * @param {string} dest A pasta onde o projeto vai ser clonado
   * @param {string} from Caminho da onde o templete é copiado
   * @param {Object} changeVarNames Variaveis a ser alteradas
   * em casa arquivo e paginas quando o template for do portal
   * @example clone('url-git', './', 'templates/template-escolhido',
   *  ['project-name', 'category-name'])
   */
  async clone(url, dest, from, changeVarNames) {
    try {
      // const { commit, cloneTemplate, createBranchs, config, spinner } = this;
      const pathFrom = path.join(from, 'template');
      const whatTemplate = this.config.repository.project_type;
      const client = new Client(this.config, this.spinner);

      this.spinner.start('Clonado repositorio do projeto..');
      await execa.shell(`git clone ${url} ${dest} -q`);
      this.spinner.succeed('Repositorio clonado com sucesso');

      await this.cloneTemplate(whatTemplate, this.config);

      await client.copy(pathFrom, dest);
      await client.remove(pathFrom);

      if (whatTemplate === 'interno') {
        await this.useFrameworkCli(this.config.repository.framework, dest);
      }
      if (whatTemplate === 'bradesco-portal') {
        //
        await client.renameVaribalesInFiles(dest, [/project-name/g, /category-name/g], changeVarNames);
        await client.rename(dest, ['project-name', 'category-name'], changeVarNames);
      }
      if ( whatTemplate === 'promocao-bradesco-portal') {
        await client.renameVaribalesInFiles(dest, [/project-name/g, /category-name/g], changeVarNames);
        await client.rename(dest, ['project-name', 'category-name'], changeVarNames);
      }
      await this.commit(dest);
      await this.createBranchs(dest);


      return {
        success: true
      };
    } catch (error) {
      console.error(`Alguma coisa deu errada: ${error}`);
      return error;
    }
  }

  /**
   * Clona o template escolhido dentro do projeto
   *
   * @public
   * @param {string} template Nome do template escolhido
   * @param {Object} config Configuração do repositorio
   */
  async cloneTemplate(template) {
    try {
      const {
        git_username,
        globaldir
      } = this.config.repository;

      const dest = path.normalize(path.join(globaldir, 'template'));
      const url = `https://${git_username}@bitbucket.org/onedigital/one-templates-${template}.git`;

      this.spinner.start('Clonando template..');
      const {
        stdout
      } = await execa.shell(`git clone ${url} ${dest} -q`);
      this.spinner.succeed('Template clonado com sucesso!');

      return stdout;
    } catch (error) {
      console.error(`Erro ao clonar o template: ${error}`);

      return error;
    }
  }

  /**
   * Caso seja um projeto interno, baixa o framework escolhido
   *
   * @public
   * @param {string} framework Nome do framework escolhido 
   * @param {string} dest Diretorio do projeto criado
   */
  async useFrameworkCli(framework, dest) {
    try {
      const client = new Client(this.config, this.spinner);
      this.spinner.start(`Baixando o ${framework} e dependências...`);

      if (framework === 'vue') {
        const inlinePreset = "{\\\"useConfigFiles\\\":true,\\\"plugins\\\":{\\\"@vue\\\/cli-plugin-babel\\\":{},\\\"@vue\\\/cli-plugin-pwa\\\":{},\\\"@vue\\\/cli-plugin-router\\\":{\\\"historyMode\\\":true},\\\"@vue\\\/cli-plugin-vuex\\\":{},\\\"@vue\\\/cli-plugin-eslint\\\":{\\\"config\\\":\\\"standard\\\",\\\"lintOn\\\":[\\\"save\\\"]},\\\"@vue\\\/cli-plugin-unit-jest\\\":{}},\\\"cssPreprocessor\\\":\\\"node-sass\\\"}"
        const {
          stdout
        } = await execa.shell(`cd ${dest} && vue create client --skipGetStarted --inlinePreset ${inlinePreset}`);
        const pathFrom = path.join(dest, 'client');
        const pathFromReadme = path.join(pathFrom, 'README.md');
        await client.remove(pathFromReadme);
        await client.copy(pathFrom, dest);
        await client.remove(pathFrom);
        this.spinner.succeed('Framework baixado com sucesso!');
        return stdout;
      }

      return true

    } catch (error) {
      console.error(`Erro ao usar o cli do framework ${framework}: ${error}`);

      return error;
    }
  }

  /**
   * Faz o primeiro commit do projeto
   *
   * @public
   * @method commit
   * @param {string} dest Diretorio do projeto criado
   * @return {Promise} Retorna uma promise logo apos o commit
   */
  async commit(dest) {
    const {
      spinner
    } = this;
    const commit = 'Adicionando a estrutura do projeto [skip ci]';
    const branch = 'master';

    try {
      spinner.start('Enviado commit..');

      const {
        stdouts
      } = await execa.shell(`cd ${dest} && git add . && git commit -am "${commit}" && git push origin ${branch}`);
      spinner.succeed('Commit efetuado com sucesso');

      return stdouts;
    } catch (err) {
      console.error(err);

      return err;
    }
  }

  /**
   * Cria as branches do projeto seguindo o git flow
   *
   * @public
   * @param {string} dest Diretorio do projeto criado
   * @return {Promise} Retorna uma promise logo apos o criar as branches
   */
  createBranchs(dest) {
    const {
      spinner
    } = this;

    spinner.start('Criando as branchs do repositorio..');

    return new Promise((resolve, reject) => {
      execSeries([
        `cd ${dest} && git checkout -b stage && git push origin stage`,
        `cd ${dest} && git checkout -b develop && git push origin develop`,
      ], (err, stdouts) => {
        if (err) {
          spinner.fail('erro na hora de criar as braches');

          reject(err);
          return;
        }

        spinner.succeed('Branchs criadas com sucesso!');

        resolve(stdouts);
      });
    });
  }
}

export default Git;
