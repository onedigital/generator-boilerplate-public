import request from 'superagent';
import fs from 'fs';
import path from 'path';

export const isTest = () => process.env.NODE_ENV !== 'test';

export const sendPostRequest = (url, token, variable) => new Promise((resolve, reject) => {
  request
    .post(url)
    .send(JSON.stringify(variable))
    .set('Authorization', `Bearer ${token}`)
    .set('Content-Type', 'application/json')
    .end((err, res) => {
      err ? reject(err) : resolve(res.body);
    });
});

export const findVariablePerKey = (variable, key) => variable.key === key;


export const readKey = keyName => fs.readFileSync(path.resolve(__dirname, `../${keyName}`)).toString();

/**
* Normaliza um caminho usando path.normalize, e remove as barras duplas
*
* @param {string} str Caminho a ser normalizado
*/
export const normalizePath = str => path.normalize(str).replace(/\\+/g, '/');
